package com.android.notes.adaptor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.notes.R;
import com.android.notes.bean.Notes;

import java.util.List;

public class NotesRecyclerAdaptor extends RecyclerView.Adapter<NotesRecyclerAdaptor.ViewHolder>{
    private final List<Notes> notesList;


    public NotesRecyclerAdaptor(List<Notes> notesList) {
        this.notesList=notesList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.title.setText(notesList.get(position).getTitle());
        holder.description.setText(notesList.get(position).getDescription());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            mOnItemClickLister.onDeleteItemClicked(view,position);
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            mOnItemClickLister.onEditItemClicked(view,position);
            }
        });
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            mOnItemClickLister.onShareItemClicked(view,position);
            }
        });

    }

    private static OnItemClickListener mOnItemClickLister;

    public interface OnItemClickListener {
        void onEditItemClicked(View view, int pos);
        void onDeleteItemClicked(View view, int pos);
        void onShareItemClicked(View view, int pos);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickLister = listener;
    }


    @Override
    public int getItemCount() {
        return notesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final TextView title;
        final TextView description;
        final ImageView delete;
        final ImageView edit;
        final ImageView share;
        ViewHolder(View itemView) {
            super(itemView);
            title=(TextView)itemView.findViewById(R.id.titleTextView);
            description=(TextView)itemView.findViewById(R.id.descTextView);
            delete=(ImageView)itemView.findViewById(R.id.delete);
            edit=(ImageView)itemView.findViewById(R.id.edit);
            share=(ImageView)itemView.findViewById(R.id.share);
        }
    }
}
