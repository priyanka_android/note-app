package com.android.notes.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.notes.R;
import com.android.notes.adaptor.NotesRecyclerAdaptor;
import com.android.notes.application.AppController;
import com.android.notes.bean.Notes;
import com.android.notes.utils.APIConstants;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class NoteListingFragment extends Fragment {
    private static final String TAG = NoteListingFragment.class.getName();


    private List<Notes> notesList;
    private NotesRecyclerAdaptor adapter;
    private RecyclerView recyclerView;
    private ProgressDialog pDialog;

    public NoteListingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_note_listing, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.notes_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        requestDataFromServer();
    }

    public void addNewNote(Notes note) {
        notesList.add(note);
        adapter.notifyDataSetChanged();
    }


    private void requestDataFromServer() {
        showPDialog();
        // Creating volley request obj
        JsonArrayRequest request = new JsonArrayRequest(APIConstants.base_URL + APIConstants.notes,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hidePDialog();
                        notesList.clear();
                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Notes model = new Notes();
                                model.setId(obj.getString("id"));
                                model.setTitle(obj.getString("title"));
                                model.setDescription(obj.getString("description"));

                                notesList.add(model);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        setAdapter(notesList);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();

            }
        });

        notesList = new ArrayList<>();
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(request);

    }

    private void setAdapter(final List<Notes> notesList) {
        adapter = new NotesRecyclerAdaptor(notesList);
        adapter.setOnItemClickListener(new NotesRecyclerAdaptor.OnItemClickListener() {
            @Override
            public void onEditItemClicked(View view, int pos) {
                showEditNotesDialog(pos, notesList.get(pos));
            }

            @Override
            public void onDeleteItemClicked(View view, int pos) {
                requestDeleteDataFromServer(pos);
            }

            @Override
            public void onShareItemClicked(View view, int pos) {
                Notes note_to_share=notesList.get(pos);
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = note_to_share.getTitle()+"\n"+note_to_share.getDescription();
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, note_to_share.getTitle());
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void requestDeleteDataFromServer(final int position) {
        showPDialog();
        StringRequest deleteRequest = new StringRequest(Request.Method.DELETE, APIConstants.base_URL + APIConstants.notes + notesList.get(position).getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        hidePDialog();
                        Log.d(TAG, "onDeleteResponse: " + response);
                        Gson gson = new Gson();
                        Notes note = gson.fromJson(response, Notes.class);
                        Toast.makeText(getActivity(), note.getTitle() + " was deleted.", Toast.LENGTH_LONG).show();
                        notesList.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error.
                        hidePDialog();
                    }
                }
        );
        AppController.getInstance().addToRequestQueue(deleteRequest);
    }

    private void requestPutdataToServer(final int position, final Notes note) {
        showPDialog();
        StringRequest putRequest = new StringRequest(Request.Method.PUT, APIConstants.base_URL + APIConstants.notes + note.getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Gson gson = new Gson();
                        Notes note_res = gson.fromJson(response, Notes.class);
                        Toast.makeText(getActivity(), "Note edited successfully.", Toast.LENGTH_LONG).show();
                        notesList.remove(position);
                        notesList.add(position, note_res);
                        adapter.notifyDataSetChanged();
                        hidePDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        hidePDialog();
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", note.getId());
                params.put("title", note.getTitle());
                params.put("description", note.getDescription());

                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(putRequest);
    }

    private void showEditNotesDialog(final int position, final Notes noteEditing) {
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setPadding(10, 10, 10, 10);

        final EditText titleBox = new EditText(getActivity());
        titleBox.setHint("Title");
        titleBox.setText(noteEditing.getTitle());
        layout.addView(titleBox);

        final EditText descriptionBox = new EditText(getActivity());
        descriptionBox.setHint("Description");
        descriptionBox.setText(noteEditing.getDescription());
        layout.addView(descriptionBox); // Another add method

        final AlertDialog alert = new AlertDialog.Builder(getActivity())
                .setView(layout)
                .setTitle(R.string.my_title)
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton(android.R.string.cancel, null)
                .create();


        alert.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = ((AlertDialog) alert).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        final String title = titleBox.getText().toString().trim();
                        final String desc = descriptionBox.getText().toString().trim();
                        if (title == null || title.trim().equalsIgnoreCase("")) {
                            titleBox.setError("Please enter title");

                        } else if (desc == null || desc.trim().equalsIgnoreCase("")) {
                            descriptionBox.setError("Please enter description");

                        } else {
                            alert.dismiss();
                            //making request
                            Notes note = new Notes();
                            note.setId(noteEditing.getId());
                            note.setTitle(title);
                            note.setDescription(desc);
                            requestPutdataToServer(position, note);

                        }


                    }
                });
            }
        });

        alert.show();
    }


    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    private void showPDialog() {
        // Showing progress dialog before making http request
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please wait...");
        pDialog.show();

    }
}
