package com.android.notes.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.notes.R;
import com.android.notes.application.AppController;
import com.android.notes.bean.Notes;
import com.android.notes.fragment.NoteListingFragment;
import com.android.notes.utils.APIConstants;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private final String TAG = MainActivity.class.getName();

    private ProgressDialog progressDialog;
    private NoteListingFragment noteListingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.addNote);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddNotesDialog();
            }
        });

        if (isOnline()) {
            Log.d(TAG, "onCreate: internet connected");
            noteListingFragment = new NoteListingFragment();
            getFragmentManager().beginTransaction().replace(R.id.container, noteListingFragment).commit();
        } else {
            Log.d(TAG, "onCreate: internet not connected");
            Toast.makeText(this, "Please connect to internet.", Toast.LENGTH_LONG).show();

        }

    }

    private void showAddNotesDialog() {
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setPadding(10, 10, 10, 10);


        final EditText titleBox = new EditText(this);
        titleBox.setHint("Title");
        layout.addView(titleBox);

        final EditText descriptionBox = new EditText(this);
        descriptionBox.setHint("Description");
        layout.addView(descriptionBox); // Another add method

        final AlertDialog alert = new AlertDialog.Builder(this)
                .setView(layout)
                .setTitle(R.string.my_title)
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton(android.R.string.cancel, null)
                .create();


        alert.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = ((AlertDialog) alert).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        final String title = titleBox.getText().toString().trim();
                        final String desc = descriptionBox.getText().toString().trim();
                        if (title == null || title.trim().equalsIgnoreCase("")) {
                            titleBox.setError("Please enter title");

                        } else if (desc == null || desc.trim().equalsIgnoreCase("")) {
                            descriptionBox.setError("Please enter description");

                        } else {
                            alert.dismiss();
                            //making request
                            requestAddNotes(title, desc);

                        }


                    }
                });
            }
        });

        alert.show();
    }

    private void requestAddNotes(final String title, final String desc) {
        showPDialog();
        StringRequest postRequest = new StringRequest(Request.Method.POST, APIConstants.base_URL + APIConstants.notes,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        hidePDialog();
                        handleResponse(response);
                        Toast.makeText(MainActivity.this, "Note added successfully.", Toast.LENGTH_LONG).show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.getMessage());
                        hidePDialog();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("title", title);
                params.put("description", desc);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(postRequest);
    }

    private void handleResponse(String response) {
        Gson gson = new Gson();
        Notes note = gson.fromJson(response, Notes.class);
        if (noteListingFragment.isAdded()) {
            noteListingFragment.addNewNote(note);
        }
    }

    private void hidePDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void showPDialog() {
        // Showing progress dialog before making http request
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }


}
